<?php
/**
 * Ручний запуск копіювання курсів
 *
 * @package    local_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/
 */


define('CLI_SCRIPT', true);

//конфігурація та ініцілізація системи
require_once(__DIR__ . '/../../config.php');
require_once("{$CFG->dirroot}/local/ilearn/lib.php");

$task = new local_ilearn\task\copy_courses;
$task->execute();