<?php
/**
 * Plugin version and basic configuration
 *
 * @package    local_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/version.php
 */

//It must be included from a Moodle page.
defined('MOODLE_INTERNAL') || die('Direct access to this script is forbidden.'); 

//Назва компоненту / Component name.
$plugin->component = 'local_ilearn';
//Версія доповнення / Plugin version.
$plugin->version = 2017082500;
//Тип релізу / Release type.
$plugin->maturity = MATURITY_ALPHA; //альфа версія.
//Версія в "людському" форматі / Version in human readable format.
$plugin->release = '1.0a';
//Вимагає версію Moodle / Required Moodle version.
$plugin->requires = 2014111002; // Moodle 2.8.2+.