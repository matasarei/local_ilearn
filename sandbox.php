<?php
/**
 * Sandbox script
 *
 * @package    local_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/
 */


define('CLI_SCRIPT', true);

//конфігурація та ініцілізація системи
require_once(__DIR__ . '/../../config.php');
require_once("{$CFG->dirroot}/local/ilearn/lib.php");

x(ilearn_is_student(915));
x(ilearn_is_teacher(915));