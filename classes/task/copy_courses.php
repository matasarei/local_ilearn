<?php
/**
 * Створення копій курсів за запитом
 *
 * @package    local_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/Task_API
 */

namespace local_ilearn\task;
require_once("{$CFG->dirroot}/course/externallib.php");
require_once("{$CFG->dirroot}/local/ilearn/lib.php");
use \core\task\scheduled_task,
    \core_course_external,
    \external_api;

class copy_courses extends scheduled_task {
    
    const CATEGORY_SORT_MULTIPLIER = 10000;
    
    /**
     * 
     * @return string
     */
    public function get_name() {
        return "Створення копій курсів за запитом";
    }
    
    public function execute() {
        global $DB, $CFG, $USER;
        
        // access hack.
        $user = $USER;
        $USER = get_admin();
        
        // category to insert.
        $category = (int)$CFG->ilearn_dest_cat;
        if (!$category) {
            return false;
        }
        
        // course order (first in the category).
        $order = ($category > 1 ? $category - 1 : $category) * self::CATEGORY_SORT_MULTIPLIER;
        
        // new requests.
        $requests = $DB->get_records_sql("SELECT * FROM {" . ILEARN_COURSES . "} WHERE status = 'new'");
        
        foreach ($requests as $request) {
            try {
                $course = $DB->get_record('course', ['id' => $request->course], '*', MUST_EXIST);
                $cuser = $DB->get_record('user', ['id' => $request->user], '*', MUST_EXIST);
                
                $name = "{$course->fullname} ({$cuser->lastname} {$cuser->firstname})";
                $shortname = "{$course->shortname} ($cuser->id)";
                
                // course id, course name (full), course name (short), destination category, visible, options (array).
                $newcourse = core_course_external::duplicate_course($course->id, $name, $shortname, $category, true, [
                    ['name' => 'activities', 'value' => true],
                    ['name' => 'blocks', 'value' => true],
                    ['name' => 'filters', 'value' => true],
                    ['name' => 'users', 'value' => false]
                ]);
                $newcourse = external_api::clean_returnvalue(core_course_external::duplicate_course_returns(), $newcourse);
                
                // fix sort (moodle sets order to 0).
                $DB->execute("UPDATE {course} SET sortorder = {$order} WHERE id = ?", [$newcourse['id']]);
                
                // update status.
                $request->status = ILEARN_COURSE_READY;
                $request->newcourse = $newcourse['id'];
                $DB->update_record(ILEARN_COURSES, $request);
                
                // enroll teacher.
                $role = $DB->get_record('role', ['id' => $CFG->ilearn_default_teacher_role]);
                $rolename = $role ? $role->shortname : 'teacher';
                ilearn_add_enrolment($cuser->id, $newcourse['id'], [$rolename]);
                
                // remove guest access.
                $DB->delete_records('enrol', ['enrol' => 'guest', 'courseid' => $newcourse['id']]);
                
            } catch (\Exception $ex) {
                trigger_error($ex->getMessage());
                $request->status = ILEARN_COURSE_ERROR;
                $DB->update_record(ILEARN_COURSES, $request);
            }
        }
        
        // return $USER value.
        $USER = $user;
        // return success flag.
        return true;
    }
    
}