<?php
/**
 * @package    local_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/
 */

namespace local_ilearn\forms;

class form_addcourse extends form_default {
    
    private $_userid;
    
    public function __construct($userid) {
        $this->_userid = $userid;
        return parent::__construct();
    }
    
    /**
     * Define form fields
     * @global type $DB
     */
    public function definition() {
        global $DB;
        $mform = $this->_form;

        $mform->addElement('hidden', 'user', $this->_userid);
        $mform->setType('user', PARAM_INT);
        
         //Заголовок форми
        $mform->addElement('header', 'descriptionhdr', get_string('add_course', 'local_ilearn'));
        $mform->addElement('html', get_string('add_course_descr', 'local_ilearn'));
        
        $options = [];
        foreach(ilearn_get_proto_courses($this->_userid) as $course) {
            $options[$course->id] = $course->fullname;
        }
        
        $mform->addElement('select', 'course', get_string('select_course', 'local_ilearn'), count($options) > 0 ? $options : [
            false => get_string('no_proto_courses', 'local_ilearn')
        ]);
        
        if ($options) {
            $mform->addGroup([
                $mform->createElement('submit', 'submitbutton', get_string('add_course', 'local_ilearn')),
            ], 'buttonarr', '', [' '], false);
        }
    }
    
}