<?php
/**
 * Розширення функціоналу форм
 * Extends forms functionality
 *
 * @package    local_gdo
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @copyright  Borys Grinchenko Kyiv University, 2016
 */

namespace local_ilearn\forms;
require_once("{$CFG->dirroot}/local/ilearn/lib.php");
require_once("{$CFG->libdir}/formslib.php");

abstract class form_default extends \moodleform {

    //field types
    const TYPE_DEFAULT = null;
    const TYPE_TEXT = 'text';
    const TYPE_GROUP = 'group';
    const TYPE_TEXTAREA = 'textarea';
    const TYPE_RADIO = 'radio';
    const TYPE_SELECT = 'select';
    const TYPE_EDITOR = 'editor';

    /**
     * Define default form fields
     * @global type $DB
     */
    public function definition() {
        $mform = $this->_form;
        
        // Back URL.
        $mform->addElement('hidden', 'back_url', null);
        $mform->setType('back_url', PARAM_URL);
        $mform->setDefault('back_url', optional_param('back_url', '\\', PARAM_URL));
    }
    
    /**
     * Створює масив полів
     * Make input array
     * @param $name string Fields name
     * @param $count int Fields count / Кількість елементів
     * @param $start int Start index / Початок масиву
     * @param $attribs array Fields attribues or select options
     * @param $title string Array title / Назва масиву
     * @param $type string Fields type (const TYPE_*)
     */
    protected function make_input_array($name, $count, $start = 1, $attribs = [], $title = '', $type = self::TYPE_TEXT)
    {
        $inputs = [];
        for ($i = $start; $i <= $count; $i++) {
            $inputs[] = $this->_form->createElement($type, "{$name}[{$i}]", $title ? "{$title} {$i}" : '', $attribs);
            $this->_form->setType("{$name}[{$i}]", PARAM_TEXT);
        }
        return $inputs;
    }

    /**
     * Set default values
     * Встановлює набір значень за замовчуванням
     * @param $data array Set of defualt values (['field' => 'value'])
     */
    public function setDefaults(array $data) {
        foreach($this->_form->_elements as $key => $element) {
            $name = empty($element->_name) ? $element->_attributes['name'] : $element->_name;
            if (isset($data[$name])) {
                $this->setDefault($name, $data[$name], $element->_type);
            }
        }
    }

    /**
     * Set default value
     * Встановлює значення за замовчуванням
     * @param $name string Field name
     * @param $value mixed Default value / values
     * @param $type string Field type (TYPE_* const)
     */
    public function setDefault($name, $value, $type = self::TYPE_DEFAULT) {
        if ($type == 'group' && is_array($value)) { //група з масивом значень
            //fix offset
            if (isset($value[0])) {
                array_unshift($value, 0);
                unset($value[0]);
            }

            foreach($value as $key => $val) {
                $this->setDefault("{$name}[{$key}]", $val);
            }
            return true;
        } elseif ($type == 'group') {  //група з одним значенням
            $value = ['value' => (int)$value];
        } elseif ($type == 'editor') { //графічний редактор
            $value = ['text' => $value];
        }
        $this->_form->setDefault($name, $value);
        return true;
    }

    public function getTableHeadings(array $table_data, $firstColumn = -1, $firstRow = -1) {
        $headings = [];
        if ($firstRow < 0) {
            $firstRow = $this->findFirstRow($table_data);
        }

        if ($firstColumn < 0) {
            $firstColumn = $this->findFirstColumn($table_data, $firstRow);
        }

        for ($i = $firstColumn; $i < count($table_data[$firstRow]); $i++) {
            if (!empty($table_data[$firstRow][$i])) {
                $headings[] = $table_data[$firstRow][$i];
            }
        }

        return $headings;
    }

    public function findFirstColumn(array $table_data, $firstRow = -1) {
        if ($firstRow < 0) {
            $firstRow = $this->findFirstRow($table_data);
        }

        foreach ($table_data[$firstRow] as $index => $cell) {
            if (!empty($cell)) {
                return $index;
            }
        }
        return false;
    }

    /**
     *
     */
    public function findFirstRow(array $table_data) {
        foreach ($table_data as $index => $row) {
            if (!is_array($row)) {
                return false;
            }
            foreach ($row as $cell) {
                if (!empty($cell)) {
                    return $index;
                }
            }
        }
        return false;
    }

    /**
     *
     */
    public function findDefault(array $options, $pattern) {
        foreach ($options as $key => $value) {
            if (is_array($pattern)) {
                foreach ($pattern as $p) {
                    if (preg_match($p, $value)) {
                        return $key;
                    }
                }
            } else {
                if (preg_match($pattern, $value)) {
                    return $key;
                }
            }
        }
        return false;
    }

}