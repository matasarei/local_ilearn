<?php
/**
 * @package    local_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/
 */

namespace local_ilearn\forms;

class form_invite_students extends form_default {
    
    private $_course;
    private $_coursename;
    private $_user;

    public function __construct($courseid, $coursename, $userid = 0) {
        $this->_course = (int)$courseid;
        $this->_coursename = (string)$coursename;
        $this->_user = (int)$userid;
        return parent::__construct();
    }
    
    /**
     * Define form fields
     * @global type $DB
     */
    public function definition() {
        
        $mform = $this->_form;

        $mform->addElement('hidden', 'course', $this->_course);
        $mform->setType('course', PARAM_INT);
        $mform->addElement('hidden', 'user', $this->_user);
        $mform->setType('user', PARAM_INT);
        
        // Заголовок форми.
        $mform->addElement('html', get_string('invite_students_descr', 'local_ilearn'));
        
        // Email адреси студентів.
        $mform->addElement('textarea', 'emails', get_string('emails', 'local_ilearn'), [
            'wrap' => 'virtual', 'rows' => '8', 'cols' => '80'
        ]);
        
        $mform->addElement('textarea', 'mail_body', get_string('mail_body', 'local_ilearn'), [
            'wrap' => 'virtual', 'rows' => '8', 'cols' => '80'
        ]);
        $this->setDefault('mail_body', get_string('invite_students_mail_body', 'local_ilearn', $this->_coursename), self::TYPE_TEXTAREA);
        
        // Дії.
        $mform->addGroup([
            $mform->createElement('submit', 'submitbutton', get_string('invite', 'local_ilearn')),
            $mform->createElement('cancel')
        ], 'buttonarr', '', [' '], false);
    }
    
}