<?php
/**
 * @package    local_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/version.php
 */

define('ILEARN_COURSES', 'ilearn_courses');
define('ILEARN_INVITE', 'ilearn_course_invite');
define('ILEARN_COURSE_NEW', 'new');
define('ILEARN_COURSE_READY', 'ready');
define('ILEARN_COURSE_ERROR', 'error');

function ilearn_require_login($backurl = null) {
    global $SESSION, $PAGE;

    if (!isloggedin() or isguestuser()) {
        if (empty($SESSION->wantsurl)) {
            $SESSION->wantsurl = empty($backurl) ? $PAGE->url : $backurl;
        }
        redirect(get_login_url());
    }
}

/**
 * Розширює блок навігації
 * Extends navigation block
 * @param global_navigation
 */
function local_ilearn_extend_navigation(global_navigation $navigation) {
    global $PAGE, $DB;
    
    $navigation->add(get_string('teachers_page', 'local_ilearn'), new moodle_url("/local/ilearn/teacher/"));
    
    $courseid = ilearn_is_course_page();
    if ($courseid) {
        $user = ilearn_get_teacher($courseid);
        $coursecontext = context_course::instance($courseid);
        if ($user && has_capability('local/ilearn:manageteachers', $coursecontext)) {
            $linkname = get_string('teachers_page', 'local_ilearn') . ' (' . fullname($user) . ')';
            $navigation->add($linkname, new moodle_url("/local/ilearn/teacher/", [
                'user' => $user->id
            ]));
        }
    }
       
}

/**
 * Check if course page
 * @global object $PAGE Page configuration object
 * @return int|boolean false or course id
 */
function ilearn_is_course_page() {
    global $PAGE;
    
    if ($PAGE->context instanceof context_course) {
        return $PAGE->course->id;
    }
    return false;
}

/**
 * 
 * @global type $DB
 * @param type $userid
 * @return type
 */
function ilearn_get_courses($userid) {
    global $DB;
    
    $records = $DB->get_records(ILEARN_COURSES, ['user' => $userid]);
    
    $courses = [];
    foreach ($records as $record) {
        $instance = new stdClass();
        $instance->id = $record->id;
        $instance->course = $DB->get_record('course', ['id' => $record->course]);
        $instance->newcourse = $DB->get_record('course', ['id' => $record->newcourse]);
        $instance->time = $record->time;
        $instance->date = date(get_string('date_format', 'local_ilearn'), $record->time);
        $instance->status = $record->status;
        $courses[] = $instance;
    }
    return $courses;
}

/**
 * 
 * @global type $DB
 * @global type $USER
 * @param type $courseid
 * @param type $userid
 * @return type
 */
function ilearn_new_course($courseid, $userid = 0) {
    global $DB, $USER;
    if (!$userid) {
        $userid = $USER->id;
    }
    
    $exist = $DB->get_record(ILEARN_COURSES, ['user' => $userid, 'course' => $courseid]);
    if ($exist) {
        return $exist->id;
    }
    
    return $DB->insert_record(ILEARN_COURSES, [
        'user' => $userid,
        'course' => $courseid,
        'newcourse' => 0,
        'time' => time(),
        'status' => ILEARN_COURSE_NEW
    ]);
}

function ilearn_get_proto_courses($userid = 0) {
    global $DB, $CFG, $USER;
    $user = $userid ? $DB->get_record('user', ['id' => $userid]) : $USER;
    
    if (!$user) {
        return [];
    }
    
    $sql = "SELECT {course}.* FROM {course}
             WHERE {course}.category = ? 
               AND {course}.visible = '1'
               AND {course}.id NOT IN (
                    SELECT course FROM {" . ILEARN_COURSES . "}
                     WHERE user = '{$user->id}'
                   )
          GROUP BY {course}.id";
    return $DB->get_records_sql($sql, [$CFG->ilearn_proto_cat]);
}

function ilearn_get_teacher($courseid) {
    global $DB;
    $sql = "SELECT {user}.* FROM {user}
        INNER JOIN {" . ILEARN_COURSES . "} AS ilearn_courses
             WHERE {user}.id = ilearn_courses.user
               AND ilearn_courses.newcourse = ?
          GROUP BY {user}.id";
    return $DB->get_record_sql($sql, [$courseid]);
}

/**
 * 
 * @global type $DB
 * @param type $course
 * @return type
 */
function ilearn_get_course_name($course) {
    global $DB;
    if (empty($course)) {
        return get_string('error', 'core');
    }
    
    if (!is_object($course)) {
        $course = $DB->get_record('course', ['id' => $course], 'id, fullname', MUST_EXIST);
    }
    
    $user = ilearn_get_teacher($course->id);
    $name_pattern = "({$user->lastname}\s*{$user->firstname}|{$user->firstname}\s*{$user->lastname})";
    return preg_replace("@\s*\({$name_pattern}\)@", "", $course->fullname, -1);
}

/**
 * User created event handler
 * @global type $DB
 * @param type $user
 * @return boolean
 */
function ilearn_user_created($user) {
    global $DB;
    
    $invites = $DB->get_records(ILEARN_INVITE, ['email' => $user->email]);
    foreach ($invites as $invite) {
        $course = $DB->get_record('course', ['id' => $invite->course]);
        if (!$course) {
            continue;
        }
        ilearn_add_enrolment($user->id, $course->id, ['student']);
    }
    return true;
}

/**
 * Course deleted event handler
 * @global type $DB
 * @param type $course
 * @return boolean
 */
function ilearn_course_removed($course) {
    global $DB;
    $DB->execute("UPDATE {ilearn_courses} SET course = '0' WHERE course = '{$course->id}'");
    
    $DB->delete_records('ilearn_courses', ['newcourse' => $course->id]);
    $DB->delete_records('ilearn_course_invite', ['course' => $course->id]);
    return true;
}

/**
 * 
 * @global type $DB
 * @param type $courseid
 * @return type
 */
function ilearn_get_invites_count($courseid) {
    global $DB;
    return $DB->count_records('ilearn_course_invite', ['course' => $courseid]);
}

/**
 * 
 * @global type $DB
 * @param type $courseid
 * @return type
 */
function ilearn_get_students_count($courseid) {
    global $DB;
    
    $sql = "SELECT COUNT(*) FROM (
            SELECT {user}.* FROM {user}
        INNER JOIN {role_assignments}
                ON {role_assignments}.userid = {user}.id
        INNER JOIN {role}
                ON {role}.id = {role_assignments}.roleid
        INNER JOIN {context}
                ON {context}.id = {role_assignments}.contextid
             WHERE {role}.archetype = 'student' 
               AND {context}.contextlevel = '50' 
               AND {context}.instanceid = ?
          GROUP BY {user}.id) AS result";
    
    $count = $DB->get_field_sql($sql, [$courseid]);
    return empty($count) ? 0 : (int)$count;
}

/**
 * 
 * @global type $DB
 * @param type $courseid
 * @return type
 */
function ilearn_get_students($courseid) {
    global $DB;
    
    $sql = "SELECT {user}.* FROM {user}
        INNER JOIN {role_assignments}
                ON {role_assignments}.userid = {user}.id
        INNER JOIN {role}
                ON {role}.id = {role_assignments}.roleid
        INNER JOIN {context}
                ON {context}.id = {role_assignments}.contextid
             WHERE {role}.archetype = 'student' 
               AND {context}.contextlevel = '50' 
               AND {context}.instanceid = ?
          GROUP BY {user}.id
          ORDER BY {role_assignments}.timemodified DESC";
    
    return $DB->get_records_sql($sql, [$courseid]);
}

/**
 * Check if user has a role
 * @global object $DB Database interface
 * @global object $USER Current user instance
 * @param int $userid User id
 * @return boolean
 */
function ilearn_has_role($userid = 0, array $roles = []) {
    global $DB, $USER;
    
    if (!$roles) {
        return false;
    }
    
    $values = [$userid ? (int)$userid : $USER->id];
    foreach ($roles as $role) {
        $values[] = (string)$role;
    }
    
    $sql = "SELECT {role_assignments}.id 
              FROM {role_assignments}
        INNER JOIN {role}
                ON {role}.id = {role_assignments}.roleid
        INNER JOIN {context}
                ON {context}.id = {role_assignments}.contextid
             WHERE {role_assignments}.userid = ?
               AND {role}.shortname IN (" . implode(",", array_fill(0, count($roles), '?')) . ")
               AND {context}.contextlevel = '50'";
    
    $records = $DB->get_records_sql($sql, $values);
    return (bool)$records;
}

/**
 * Check if user is a student
 * @param int $userid
 * @return boolean
 */
function ilearn_is_student($userid = 0) {
    return ilearn_has_role($userid, ['student']);
}

/**
 * Check if user id a teacher
 * @param int $userid
 * @return boolean
 */
function ilearn_is_teacher($userid = 0) {
    return ilearn_has_role($userid, ['teacher', 'editingteacher']);
}

/**
 * 
 * @global type $DB
 * @param int $userid User id
 * @param type $courseid Course id
 * @param array $roles Shortnames of roles
 * @return array Enrollments as assignments data
 */
function ilearn_get_enrolments($userid, $courseid, array $roles) {
    global $DB;
    
    $values = [(int)$userid, (int)$courseid];
    foreach ($roles as $role) {
        $values[] = (string)$role;
    }
    $values[] = (int)$userid;
    
    $sql = "SELECT {user_enrolments}.id AS enrolment_id, 
                   {role_assignments}.id AS assignment_id,
                   {role}.id AS role_id,
                   {role}.shortname AS role_shortname,
                   {enrol}.id AS enrol_id,
                   {role_assignments}.timemodified AS timemodified,
                   {user_enrolments}.timestart AS timestart,
                   {user_enrolments}.timeend AS timeend
              FROM {role_assignments}
        INNER JOIN {context} ON {role_assignments}.contextid = {context}.id
        INNER JOIN {role} ON {role_assignments}.roleid = {role}.id
        INNER JOIN {enrol} 
                ON {enrol}.courseid = {context}.instanceid
               AND {enrol}.enrol = 'manual'
        INNER JOIN {user_enrolments} 
                ON {user_enrolments}.enrolid = {enrol}.id
               AND {user_enrolments}.userid = ?
             WHERE {context}.contextlevel = 50 
               AND {context}.instanceid = ?
               AND {role}.shortname IN (" . implode(", ", array_fill(0, count($roles), '?')) . ")
               AND {role_assignments}.userid = ?
          GROUP BY {role_assignments}.id";
    return $DB->get_records_sql($sql, $values);
}

/**
 * Check user enrollment
 * @param int $userid User id
 * @param type $courseid Course id
 * @param array $roles Shortnames of roles
 * @return int Count of enrolments per role
 */
function ilearn_check_enrolment($userid, $courseid, array $roles) {
    return count(ilearn_get_enrolments($userid, $courseid, $roles));
}

/**
 * Remove user enrollment
 * @global type $DB
 * @param int $userid User id
 * @param type $courseid Course id
 * @param array $roles Shortnames of roles
 * @return mixed Count of removed registrations or FALSE
 */
function ilearn_remove_enrolment($userid, $courseid, array $roles) {
    global $DB;
    $count = false;
    
    $enrolments = ilearn_get_enrolments($userid, $courseid, $roles);
    foreach ($enrolments as $enrolment) {
        $DB->delete_records('role_assignments', ['id' => $enrolment->assignment_id]);
        $DB->delete_records('user_enrolments', ['id' => $enrolment->enrolment_id]);
        $count = $count === false ? 1 : $count + 1;
    }
    return $count;
}

/**
 * Enroll user to a course using manual enrol helper
 * @global type $DB
 * @param int $userid User id
 * @param int $courseid Course id
 * @param array $roles Shortnames of roles
 * @return mixed Count of success registrations or FALSE
 */
function ilearn_add_enrolment($userid, $courseid, array $roles) {
    global $DB;
    $count = false;
    
    // enrol plugin.
    if (!$enrol_manual = enrol_get_plugin('manual')) {
        throw new coding_exception('Can not instantiate enrol helper');
    }
    
    // enroll instance.
    $instance = $DB->get_record('enrol', [
            'courseid' => $courseid, 'enrol' => 'manual'
    ], '*', MUST_EXIST);
    
    $enroltime = time() - 86400; // 86400 = day in seconds.
    
    // enrol foreach role.
    foreach ($roles as $role) {
        if (ilearn_check_enrolment($userid, $courseid, [$role])) {
            $count = $count === false ? 1 : $count + 1;
            continue;
        }
        
        $roleid = $DB->get_field('role', 'id', ['shortname' => (string)$role], MUST_EXIST);
        $enrol_manual->enrol_user($instance, $userid, $roleid, $enroltime, 0);
        
        $count = $count === false ? 1 : $count + 1;
    }
    return $count;
}

/**
 * Debug function
 * @param mixed param1
 * @param mixed param2
 * @param mixed param3
 * ...
 */
if (!function_exists('x')) {
    function x() {
        $backtrace = debug_backtrace();
        $file = empty($backtrace[0]['file']) ? false : $backtrace[0]['file'];
        if ($file) {
            $line = empty($backtrace[0]['line']) ? 0 : $backtrace[0]['line'];
            $fileinfo = "[D] {$file} on line {$line}";
            if (CLI_SCRIPT) {
                fwrite(STDERR, $fileinfo);
                fwrite(STDERR, PHP_EOL);

            } else {
                echo html_writer::tag('div', $fileinfo);
            }
        }

        $args = func_get_args();
        foreach ($args as $n => $arg) {
            print_object($arg);
        }
    }
}