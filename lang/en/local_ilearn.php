<?php
/**
 * Languange strings
 *
 * @package    local_gdo
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/
 */

///
// General.
///
$string['pluginname'] = "iLearn";
$string['title'] = "iLearn";
$string['date_format'] = 'd.m.Y';

$string['emails'] = "E-Mail адреси";
$string['mail_body'] = 'Текст запрошення';
$string['invite'] = "Запросити";
$string['teachers_page'] = "Сторінка викладача";
$string['become_teacher'] = "Стати викладачем";
$string['no_courses'] = "Не додано жодного курсу";
$string['add_course'] = "Додати курс";
$string['select_course'] = 'Оберіть курс';
$string['settings_heading'] = "Налаштування iLearn";
$string['proto_category'] = "Категорія з 'шаблонами курсів'";
$string['proto_category_descr'] = "Категорія курсів, що будуть доступні для наслідування / копіювання.";
$string['remove_course'] = "Видалити курс";

$string['add_course_descr'] = "<b style='color:red'>Увага, інструкція!</b> "
        . "Ця дія реєструє запит на створення курсу за шаблоном з переліку. Ваш запит стає в чергу і отримує статус <i>\"Новий\"</i>, "
        . "новий курс буде створено протягом години. Після завершення запит отримає статус <i>\"Готовий\"</i>, після чого Ви отримаєте "
        . "повний доступ до курсу, а також доступ до додаткових дій.";

$string['course_status_new'] = "Новий";
$string['course_status_ready'] = "Готовий";
$string['course_status_error'] = "Помилка!";
$string['course_removed'] = 'Курс видалено!';

$string['invite_students'] = 'Запросити студентів';
$string['invite_students_title'] = 'Запросити студентів: {$a}';
$string['invite_students_descr'] =  "<b style='color:red'>Увага, інструкція!</b> "
        . "Введіть E-Mail адреси студентів, що будуть зараховані на курс <i>(через кому, або з нового рядка)</i>. Студенти, що не зареєстровані "
        . "в системі отримають запрошення на елекронну пошту, та будуть зараховані на курс одразу після реєстрації. "
        . "Вже зареєстровані користувачі також отримають запрошення, та будуть зараховані на курс після підтвердження.";
$string['invite_students_mail_body'] = 'Вітаю! Запрошую вас на свій курс {$a}. Перейдіть за посиланням, щоб зареєструватися.';
$string['invite_mail_fail'] = 'Не владося віправити запрошення на адресу {$a}';

$string['dest_category'] = "Категорія копій";
$string['dest_category_descr'] = "Категорія для збереження копій курсів.";

$string['default_teacher_role'] = "Типова роль викладача";
$string['default_teacher_role_descr'] = "Роль, що автоматично встановлюється при копіюванні курсів, тощо.";

$string['course_invite'] = "Запрошення на курс";
$string['accept_invitation'] = "Прийняти запрошення";
$string['invitation_sended'] = "Запрошення відправлене";
$string['invitation_wronguser'] = "Ви не можете прийняти це запрошення, так як воно належить іншому користувачу.";
$string['invitation_success'] = "Ви зараховані на курс. Успішного навчання!";
$string['invitation_cancelled'] = "Запрошення скасовано";
$string['cancel_invitation'] = "Скасувати запрошення";
$string['invitations_list'] = "Перелік запрошень";
$string['noinvitations'] = "Актуальні запрошення відсутні";
$string['view_invites'] = "Перегляути запрошення";
$string['invite_self_error'] = "Ви не можете запросити себе.";
$string['invitation_removed'] = "Запрошення видалено";
$string['enroled_students'] = "Зараховані студенти";
$string['noenroled_students'] = "Да даний момент зараховані студенти відсутні.";

$string['students_count_descr'] = "Кількість зарахованих студентів (в дужках кількість запрошенних студентів).";

$string['course_remove_forbidden'] = "Заборонено видаляти діючі курси. Зверніться до адміністратора.";
$stting['course_removed'] = "Курс видалено.";

$string['teacher_only_descr'] = "Тільки викладачі мають доступ до цієї сторінки.";

$string['teacher_invite'] = "Ви вчитель і бажаєте використовувати курси iLearn <br> на власних уроках зі своїми учнями?";
$string['teacher_invite_btn'] = "Долучайтеся!";

$string['no_proto_courses'] = "Немає доступних курсів";

$string['manage_enrollments'] = "Управління зарахованими користувачами";