<?php
/**
 * Database events handlers
 *
 * @package    local_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 */

$handlers = [
    'user_created' => [
        'handlerfile'      => '/local/ilearn/lib.php',
        'handlerfunction'  => 'ilearn_user_created',
        'schedule'         => 'instant',
        'internal'         => 1,
    ],
    'course_deleted' => [
        'handlerfile'      => '/local/ilearn/lib.php',
        'handlerfunction'  => 'ilearn_course_removed',
        'schedule'         => 'instant',
        'internal'         => 1,
    ]
];