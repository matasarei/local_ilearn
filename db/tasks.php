<?php
/**
 * Tasks (cron) configuration
 *
 * https://docs.moodle.org/dev/Task_API
 *
 * @package    local_gdo
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @copyright  Borys Grinchenko Kyiv University, 2016
 */

//groups sync
$tasks = [
    [
        'classname' => 'local_ilearn\task\copy_courses',
        'blocking' => 0, //block task if faided (seconds)
        'minute' => '1', // first minute of every hour
        'hour' => '*',
        'day' => '*',
        'dayofweek' => '*',
        'month' => '*'
    ],
];