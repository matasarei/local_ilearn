<?php
/**
 * http://docs.moodle.org/dev/Upgrade_API
 * http://docs.moodle.org/dev/Data_definition_API
 * http://docs.moodle.org/dev/XMLDB_creating_new_DDL_functions
 * http://docs.moodle.org/dev/DB_layer_2.0_migration_docs
 *
 * WARNING! Since Moodle 2.0 enum support is TOTALY dropped! Use char instead.
 * Зверніть увагу, що з версії 2.0 розробники вирішили ПОВНІСТЮ відмовитися від enum типу.
 * Це означає що усі подібні поля необхідно замінити на звичайні текстові.
 *
 * @package    local_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/
 */

// It must be included from a Moodle page.
defined('MOODLE_INTERNAL') || die('Direct access to this script is forbidden.'); 

/**
 * Performs upgrade of the database structure and data
 *
 * @param int $oldversion the version we are upgrading from
 * @return bool true
 */
function xmldb_local_ilearn_upgrade($oldversion = 0) {
    global $DB;

    $dbman = $DB->get_manager();
    
    if ($oldversion < 2017080400) {
        $table = new xmldb_table('ilearn_courses');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        
        // user / користувач
        $table->add_field('user', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL);
        
        // original course / ід оригінального курсу
        $table->add_field('course', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL);
        
        // new course (copy) / ід нового курсу
        $table->add_field('newcourse', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL);

        // time / час створення
        $table->add_field('time', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL);
        
        // status / статус обробки
        $table->add_field('status', XMLDB_TYPE_CHAR, '32', null, XMLDB_NOTNULL);
        
        // setup primary key.
        $primary = new xmldb_key('primary');
        $primary->set_attributes(XMLDB_KEY_PRIMARY, ['id'], null, null);
        $table->addKey($primary);

        $dbman->create_table($table);
    }
    
    if ($oldversion < 2017081800) {
        $table = new xmldb_table('ilearn_course_invite');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        
        // course / ід курсу
        $table->add_field('course', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL);
        
        // email
        $table->add_field('email', XMLDB_TYPE_CHAR, '64', null, XMLDB_NOTNULL);
        
        // setup primary key.
        $primary = new xmldb_key('primary');
        $primary->set_attributes(XMLDB_KEY_PRIMARY, ['id'], null, null);
        $table->addKey($primary);

        $dbman->create_table($table);
    }
    
    return true;
}