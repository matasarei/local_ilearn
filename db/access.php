<?php
/**
 * Definition of capabilities
 * Визначення можливостей користувачів виконувати певні дії
 *
 * http://docs.moodle.org/dev/Access_API
 * http://docs.moodle.org/dev/Role_archetypes
 * http://docs.moodle.org/dev/Hardening_new_Roles_system
 * http://rus-linux.net/MyLDP/BOOKS/Architecture-Open-Source-Applications/Vol-2/moodle-2.html
 *
 * @package    local_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 */

///
// Context level tip
//
// CONTEXT_SYSTEM = 10
// CONTEXT_PERSONAL = 20
// CONTEXT_USER = 30
// CONTEXT_COURSECAT = 40
// CONTEXT_COURSE = 50
// CONTEXT_GROUP = 60 ?? !! DO NOT USE
// CONTEXT_MODULE = 70
// CONTEXT_BLOCK = 80
///

///
// Risks tip
//
// RISK_SPAM - user can add visible content to site, send messages to other users; originally protected by !isguest()
// RISK_PERSONAL - access to private personal information - ex: backups with user details, non public information in profile (hidden email), etc.; originally protected by isteacher()
// RISK_XSS - user can submit content that is not cleaned (both HTML with active content and unprotected files); originally protected by isteacher()
// RISK_CONFIG - user can change global configuration, actions are missing sanity checks
// RISK_MANAGETRUST - manage trust bitmasks of other users
// RISK_DATALOSS - can destroy large amounts of information that cannot easily be recovered.
///

$capabilities = [

    // керування викладачами
    'local/ilearn:manageteachers' => [
        'riskbitmask' => RISK_DATALOSS,
        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => [
            'manager' => CAP_ALLOW
        ]
    ],
    
];