<?php
/**
 * Сторінка налаштувань / settings page
 *
 * @package    local_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/
 */

// It must be included from a Moodle page.
defined('MOODLE_INTERNAL') || die('Direct access to this script is forbidden.');

/**
 * Налаштування плагіну (Управління->Плагіни(Модулі)->Локальні...)
 */
if ($hassiteconfig) {
    
    // Створюємо сторінку налаштувань.
    $settings = new admin_settingpage('local_ilearn', get_string('settings_heading', 'local_ilearn'));
    $ADMIN->add('localplugins', $settings);
    
    $course_cats = [];
    foreach ($DB->get_records('course_categories') as $cat) {
        $course_cats[$cat->id] = $cat->name;
    }
    
    // Категорія курсів для "наслідування".
    $settings->add(new admin_setting_configselect('ilearn_proto_cat', get_string('proto_category', 'local_ilearn'), get_string('proto_category_descr', 'local_ilearn'), PARAM_INT, $course_cats));

    // Категорія курсів для створення "наслідуваних" курсів. 
    $settings->add(new admin_setting_configselect('ilearn_dest_cat', get_string('dest_category', 'local_ilearn'), get_string('dest_category_descr', 'local_ilearn'), PARAM_INT, $course_cats));
    
    $roles = [];
    foreach ($DB->get_records('role') as $role) {
        $roles[$role->id] = "{$role->shortname} (" . get_string("legacy:{$role->archetype}", 'core_role') . ')';
    }
    
    // Категорія курсів для "наслідування".
    $settings->add(new admin_setting_configselect('ilearn_default_teacher_role', get_string('default_teacher_role', 'local_ilearn'), get_string('default_teacher_role_descr', 'local_ilearn'), PARAM_INT, $roles));

}