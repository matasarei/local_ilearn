<?php
/**
 * @package    local_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/
 */

// Config and system init.
require_once(__DIR__ . '/../../config.php');
require_once("{$CFG->dirroot}/local/ilearn/lib.php");

// Check access.
require_login();

$invite_id = optional_param('id', 0, PARAM_INT);
$invite = $DB->get_record('ilearn_course_invite', ['id' => $invite_id]);

if (!$invite) {
    redirect(new moodle_url('/'), get_string('invitation_cancelled', 'local_ilearn'), 0);
}

if ($USER->email === $invite->email) {
    
    ilearn_add_enrolment($USER->id, $invite->course, ['student']);
    
    $DB->delete_records('ilearn_course_invite', ['id' => $invite->id]);
    
    redirect(new moodle_url('/course/view.php', [
        'id' => $invite->course
    ]), get_string('invitation_success', 'local_ilearn'), 10);
    
} else {
    redirect(new moodle_url('/'), get_string('invitation_wronguser', 'local_ilearn'), 30);
}
