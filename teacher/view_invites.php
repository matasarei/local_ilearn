<?php
/**
 * @package    local_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/
 */

// Config and system init.
require_once(__DIR__ . '/../../../config.php');

// Check access.
require_login();

// Get course.
$userid = optional_param('user', 0, PARAM_INT);
if (!$userid) {
    redirect(new moodle_url('/'));
}
$user = $DB->get_record('user', ['id' => $userid], '*', MUST_EXIST);
$course = $DB->get_record('course', [
    'id' => optional_param('course', 0, PARAM_INT)
], '*', MUST_EXIST);

// Setting up page.
$PAGE->set_context(context_course::instance($course->id));
$PAGE->set_url("/local/ilearn/teacher/invite_students.php");
$title = get_string('invite_students_title', 'local_ilearn', $course->fullname);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout("course");

echo $OUTPUT->header();

$invites = $DB->get_records('ilearn_course_invite', ['course' => $course->id]);

echo $OUTPUT->heading(get_string('invitations_list', 'local_ilearn'));

if ($invites) {
    $list = [];
    foreach ($invites as $invite) {
        $cancel_url = new moodle_url('/local/ilearn/teacher/cancel_invitation.php', [
            'course' => $course->id,
            'user' => $user->id,
            'invite' => $invite->id
        ]);
        $link = html_writer::link($cancel_url, get_string('cancel_invitation', 'local_ilearn'), [
            'style' => 'color:red'
        ]);
        $list[] = "{$invite->email} ({$link})";
    }
    echo html_writer::alist($list);
} else {
    
    echo $OUTPUT->notification(get_string('noinvitations', 'local_ilearn'));
}


$enroled = ilearn_get_students($course->id);
echo $OUTPUT->heading(get_string('enroled_students', 'local_ilearn'));
if ($enroled) {
    
    $manage_url = new moodle_url('/enrol/users.php', ['id' => $course->id]);
    echo html_writer::link($manage_url, '[' . get_string('manage_enrollments', 'local_ilearn') . ']');
    
    $table = new html_table();
    $table->head = ['id', get_string('name'), 'E-Mail', ''];
    $table->size = ['10%', '30%', '30%', '30%'];
    $table->data = [];
    $table->attributes = ['class' => 'generaltable fullwidth'];
    foreach ($enroled as $student) {
        $table->data[] = [
            $student->id,
            fullname($student),
            $student->email,
            html_writer::link(new moodle_url('/user/profile.php', [
                'id' => $student->id
            ]), '[' . get_string('profile') . ']')
        ];
    }
    echo html_writer::table($table);
    
} else {
    echo $OUTPUT->notification(get_string('noenroled_students', 'local_ilearn'));
}

echo $OUTPUT->footer();