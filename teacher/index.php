<?php
/**
 * @package    local_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/
 */

// Config and system init.
require_once(__DIR__ . '/../../../config.php');
use local_ilearn\forms\form_addcourse;

// Get user.
$userid = optional_param('user', 0, PARAM_INT);
$user = $userid ? $DB->get_record('user', ['id' => $userid], '*', MUST_EXIST) : $USER;

// Setting up page.
$PAGE->set_context(context_system::instance());
$PAGE->set_url("/local/ilearn/teacher/");
$PAGE->set_title(fullname($user));
$PAGE->set_heading(fullname($user));
$PAGE->set_pagelayout("coursecategory");

// Check access.
if ($userid && $userid != $USER->id) {
    require_capability('local/ilearn:manageteachers', $PAGE->context);
}

if (!isloggedin() or isguestuser()) {
    if (empty($SESSION->wantsurl)) {
        $SESSION->wantsurl = $PAGE->url;
    }
    redirect(get_login_url());
}

$form = new form_addcourse($user->id);
$data = $form->get_data();
if ($data) {
    ilearn_new_course($data->course, $data->user);
    echo redirect(new moodle_url($PAGE, ['user' => $user->id]));
}

$courses = ilearn_get_courses($user->id);

echo $OUTPUT->header();
if (ilearn_is_student($userid) && !ilearn_is_teacher($userid)) {
    echo redirect("/", get_string('teacher_only_descr', 'local_ilearn'), 10);
}

$heading = get_string('teachers_page', 'local_ilearn');
if ($USER->id !== $user->id) {
    $heading .= ': ' . fullname($user); 
}
echo $OUTPUT->heading($heading);

$table = new html_table();
$table->size = ['30%', '10%', '10%', '10%', '40%'];
$table->attributes['style'] = "min-width:80%";
$table->head = [
    get_string('course'),
    get_string('date'),
    get_string('status'),
    get_string('students'),
    get_string('actions')
];
if ($courses) {
    $text_removed = html_writer::tag('span', get_string('course_removed', 'local_ilearn'), [
        'style' => 'color:red'
    ]);
    foreach ($courses as $row) {
        
        $actions = [];
        
        if ($row->status === 'ready') {
            $url = new moodle_url('/course/view.php', ['id' => $row->newcourse->id]);
            $name = html_writer::link($url, $row->newcourse->fullname);
            
            $actions[] = html_writer::link(new moodle_url('/local/ilearn/teacher/invite_students.php', [
                'course' => $row->newcourse->id,
                'user' => $user->id
            ]), '[' . get_string('invite_students', 'local_ilearn') . ']');
            
            $actions[] = html_writer::link(new moodle_url('/local/ilearn/teacher/view_invites.php', [
                'course' => $row->newcourse->id,
                'user' => $user->id
            ]), '[' . get_string('view_invites', 'local_ilearn') . ']');
            
            $students = 0;
        } else {
            
            $actions[] = html_writer::link(new moodle_url('/local/ilearn/teacher/remove_course.php', [
                'id' => $row->id
            ]), '[' . get_string('remove_course', 'local_ilearn') . ']', ['style' => 'color:red']);
            
            $name = $row->course ? $row->course->fullname : $text_removed;
            $students = '-';
        }
        if ($row->newcourse) {
            $name = html_writer::link(new moodle_url('/course/view.php', [
                'id' => $row->newcourse->id
            ]), ilearn_get_course_name($row->newcourse));
        } else {
            $name = $row->course->fullname;
        }
        
        $stud_count = $row->newcourse ? ilearn_get_students_count($row->newcourse->id) : 0;
        $invite_count = $row->newcourse ? '(' . ilearn_get_invites_count($row->newcourse->id) . ')' : '';
        
        $table->data[] = [
            $name,
            $row->date,
            get_string("course_status_{$row->status}", 'local_ilearn'),
            html_writer::tag('abbr', "{$stud_count} {$invite_count}", [
                'title' => get_string('students_count_descr', 'local_ilearn')
            ]),
            implode(' | ', $actions)
        ];
    }
    echo html_writer::table($table);
} else {
    echo $OUTPUT->notification(get_string('no_courses', 'local_ilearn'));
}


echo html_writer::empty_tag('hr');
$form->display();
echo $OUTPUT->footer();