<?php
/**
 * @package    local_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/
 */

// Config and system init.
require_once(__DIR__ . '/../../../config.php');
require_once("{$CFG->dirroot}/local/ilearn/lib.php");
use local_ilearn\forms\form_invite_students;

// Check access.
require_login();

// Get course.
$userid = optional_param('user', 0, PARAM_INT);
if (!$userid) {
    redirect(new moodle_url('/'));
}
$user = $DB->get_record('user', ['id' => $userid], '*', MUST_EXIST);
$course = $DB->get_record('course', [
    'id' => optional_param('course', 0, PARAM_INT)
], '*', MUST_EXIST);

// Setting up page.
$PAGE->set_context(context_course::instance($course->id));
$PAGE->set_url("/local/ilearn/teacher/invite_students.php");
$title = get_string('invite_students_title', 'local_ilearn', $course->fullname);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout("course");

$form = new form_invite_students($course->id, ilearn_get_course_name($course), $user->id);
if ($form->is_cancelled()) {
    redirect(new moodle_url('/local/ilearn/teacher/', [
        'user' => $user->id
    ]), get_string('cancelled'), 0);
}

$data = $form->get_data();

echo $OUTPUT->header();
echo $OUTPUT->heading($title);
echo html_writer::empty_tag('hr');
if ($data) {
    $emails = array_unique(preg_split("@[\r\n\,\;]@", $data->emails, -1, PREG_SPLIT_NO_EMPTY));
    
    foreach ($emails as $email) {
        if ($user->email === $email) {
            echo $OUTPUT->notification(get_string('invite_self_error', 'local_ilearn'));
            continue;
        }
        
        $to = $DB->get_record('user', ['email' => $email]);
        
        $exist = $DB->get_record('ilearn_course_invite', ['email' => $email, 'course' => $course->id]);
        if (!$exist) {
            $record = new stdClass();
            $record->course = $course->id;
            $record->email = $email;
            $invite_id = $DB->insert_record('ilearn_course_invite', $record);
        }
        
        if (!$to) {
            $to = new stdClass();
            $to->id = -1;
            $to->firstname = $email;
            $to->lastname  = '';
            $to->email = $email;
            
            $invite_url = new moodle_url('/login/signup.php');
        } else {
            
            $invite_url = new moodle_url('/local/ilearn/accept_invitation.php', [
                'id' => $exist ? $exist->id : $invite_id
            ]);
        }
        
        $msg = "{$data->mail_body} " . html_writer::link($invite_url, get_string('accept_invitation', 'local_ilearn'));

        // Use PHPMailer to send email.
        $mail = get_mailer();
        $mail->From = $user->email;
        $mail->FromName = fullname($user);
        $mail->AddAddress($email);
        $mail->Subject = get_string('course_invite', 'local_ilearn') . ' "' . ilearn_get_course_name($course) . '"';
        $mail->Body = $msg;
        $mail->IsHTML(true);
        if (!$mail->Send()) {
            echo $OUTPUT->notification(get_string('invite_mail_fail', 'local_ilearn', [$email]));
        }
    }
    
    redirect(new moodle_url('/local/ilearn/teacher/', [
        'user' => $userid
    ]), get_string('invitation_sended', 'local_ilearn'), 30);
} else {
    $form->display();
}
echo $OUTPUT->footer();