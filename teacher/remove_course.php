<?php
/**
 * @package    local_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/
 */

// Config and system init.
require_once(__DIR__ . '/../../../config.php');

// Check access.
require_login();

// Get user.
$recordid = optional_param('id', 0, PARAM_INT);

$record = $DB->get_record('ilearn_courses', ['id' => $recordid]);
if ($record->status === 'ready') {
    redirect(new moodle_url('/local/ilearn/teacher/', [
        'user' => $record->user
    ], get_string('course_remove_forbidden', 'local_ilearn')));
}

$DB->delete_records('ilearn_courses', ['id' => $record->id]);
$DB->delete_records('ilearn_course_invite', ['course' => $courseid]);

redirect(new moodle_url('/local/ilearn/teacher/', [
    'user' => $record->user
], get_string('course_removed', 'local_ilearn')));