<?php
/**
 * @package    local_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/
 */

// Config and system init.
require_once(__DIR__ . '/../../../config.php');

// Check access.
require_login();

// Get course.
$userid = optional_param('user', 0, PARAM_INT);
if (!$userid) {
    redirect(new moodle_url('/'));
}
$user = $DB->get_record('user', ['id' => $userid], '*', MUST_EXIST);
$course = $DB->get_record('course', [
    'id' => optional_param('course', 0, PARAM_INT)
], '*', MUST_EXIST);
$invite = $DB->get_record('ilearn_course_invite', [
    'id' => optional_param('invite', 0, PARAM_INT)
], '*', MUST_EXIST);

$DB->delete_records('ilearn_course_invite', ['id' => $invite->id]);
redirect(new moodle_url('/local/ilearn/teacher/view_invites.php', [
    'course' => $course->id,
    'user' => $user->id
]), get_string('invitation_removed', 'local_ilearn'), 5);